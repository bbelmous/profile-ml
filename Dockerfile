FROM python:slim

# install basic utilities
RUN \
  apt-get -qq -y update && \
  apt-get -qq -y upgrade && \
  apt-get install -y jq git tree bash-completion wget && \
  apt-get install -y sudo && \
  apt-get install -y cmake valgrind gcc build-essential && \
  apt-get install -y libeigen3-dev libboost-dev && \
  apt-get clean && \
  rm -rf /var/lib/apt-get/lists/* && \
  true

# install additional things (if they become stable move to the above list)
RUN \
  true

# compile anything that needs compiling here
WORKDIR /docker
COPY build-lwtnn.sh .
RUN \
  ./build-lwtnn.sh && \
  true

# user setup: let everyone access sudo with no password
RUN \
  echo '%sudo	ALL=(ALL)	NOPASSWD: ALL' >> /etc/sudoers && \
  useradd -m atlas && \
  usermod -aG sudo atlas && \
  true
ENV USER=atlas HOME=/home/atlas
COPY custom-rc.bash ${HOME}/.bashrc

# env setup
USER atlas
ENV HISTFILE=${HOME}/work/.bash_history
WORKDIR ${HOME}/work

CMD /bin/bash --rcfile ${HOME}/.bashrc
