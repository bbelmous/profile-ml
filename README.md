Docker image for callgrind
==========================

This is some code to build a docker image that contains callgrind.

Quick Start
===========

There are a few ways to run this image. I recommend the local docker
method, but I've included others for convenience.

Local docker image
------------------

First follow the [instructions to install docker][getdocker]. Once you
have docker installed, you should be able to get the image by running

```
docker pull gitlab-registry.cern.ch/aml/qt/younes/containers/profile-ml:latest
docker tag gitlab-registry.cern.ch/aml/qt/younes/containers/profile-ml:latest profile-ml:latest
```

where the second command is a way to alias the full container URL to something which is easier to work with locally.

In principal you can launch an interactive job now with

```
docker run --rm -it -v ${PWD}:/home/atlas/work profile-ml:latest bash
```

where `-v ${PWD}:/home/atlas/work` tells docker to mount your current
directory in `/home/atlas/work`. Note that _only changes in this
directory will be saved_ when you exit. You also won't have access to
your ssh credentials, which can make interactive work cumbersome.

I've tried to automate some of the steps of setting up a more
functional interactive session, you can try this out with the
`run-docker.sh` script in this repository.

[getdocker]: https://docs.docker.com/get-docker/

With Singularity
----------------

Most clusters don't have docker installed, but you can use singularity
instead.

```
singularity exec '/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/aml/qt/younes/containers/profile-ml:latest' bash
```

On a cluster with cvmfs
-----------------------

If you have cvmfs on your cluster, you can also use the
["unpacked" images][up], which will be a lot faster to load than
singularity. Something like this should work:

```
singularity exec '/cvmfs/unpacked.cern.ch/registry.hub.docker.com/aml/qt/younes/containers/profile-ml:latest' bash
```

[up]: https://gitlab.cern.ch/unpacked/sync


Forking this code
=================

This code should be relatively easy to fork and modify:

   - Fork this repo
   - Wait for ci to build
      + **NOTE:** this might not happen automatically, you can trigger
        manually by clicking on the "CI/CD" tab on the left and then
        pushing "Run Pipeline" on the top


Adding New Things
=================

You're encouraged to fork this and add whatever you need.

Edit the `Dockerfile` and add a call to `apt-get install -y <package>`

Building locally
----------------

The `build-docker.sh` script should build a local copy of the
repository. It can be run with `run-docker.sh` as above, docker will
take the most recent version. This is a good way to test any changes
you may have made to `Dockerfile`.


Thanks
======

Thanks to Matthew Feickert for his [example images][1], most things
are copied from there.

[1]: https://gitlab.cern.ch/aml/containers/docker
